provider "aws" {
  region = "YOUR_AWS_REGION"
}

data "aws_eks_cluster" "cluster" {
  name = "YOUR_EKS_CLUSTER_NAME"
}

module "nginx" {
  source = "terraform-aws-modules/kubernetes-nginx-ingress/aws"

  name = "nginx-ingress"

  kubernetes_cluster_id = data.aws_eks_cluster.cluster.id
}

resource "kubernetes_deployment" "nginx" {
  metadata {
    name = "nginx"
    labels = {
      app = "nginx"
    }
  }

  spec {
    selector {
      match_labels = {
        app = "nginx"
      }
    }

    template {
      metadata {
        labels = {
          app = "nginx"
        }
      }

      spec {
        container {
          name = "nginx"
          image = "nginx"
          port {
            container_port = 80
          }
        }
      }
    }
  }
}

resource "kubernetes_secret" "nginx-ssl" {
  metadata {
    name = "nginx-ssl"
  }
  data = {
    tls.crt = filebase64("path/to/cert.crt")
    tls.key = filebase64("path/to/cert.key")
  }
  type = "kubernetes.io/tls"
}

resource "kubernetes_service" "nginx" {
  metadata {
    name = "nginx"
  }

  spec {
    selector = {
      app = kubernetes_deployment.nginx.spec.template.metadata.labels.app
    }

    port {
      name = "http"
      port = 80
      target_port = 80
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_ingress" "nginx" {
  metadata {
    name = "nginx"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
      "nginx.ingress.kubernetes.io/ssl-redirect" = "true"
      "nginx.ingress.kubernetes.io/ssl-passthrough" = "true"
      "nginx.ingress.kubernetes.io/backend-protocol" = "HTTPS"
    }
  }

  spec {
    tls {
      secret_name = kubernetes_secret.nginx-ssl.metadata.name
    }

    rule {
      http {
        paths {
          path = "/"
          backend {
            service_name = kubernetes_service.nginx.metadata.name
            service_port = kubernetes_service.nginx.spec.port.name
          }
        }
      }
    }
  }
}

output "nginx_endpoint" {
  value = kubernetes_ingress.nginx.status.load_balancer.ingress[0].hostname
}



# AWS region and EKS cluster name are not specified and need to be replaced with the actual values. Also, the SSL certificate and key files need to be provided and their paths updated accordingly.